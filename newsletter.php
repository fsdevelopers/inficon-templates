<!DOCTYPE html>
<html>
<head>
	<title>Newsletter</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
</head>
	<body>
		<?php
			include "partials/header.php";
			include "partials/content-newsletter.php";
			include "partials/footer.php";
		?>
	</body>
</html>
