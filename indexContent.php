<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />

</head>
	<body>
		<?php
			include "partials/header.php";
			include "partials/content.php";
			include "partials/footer.php";
		?>


	</body>
</html>
