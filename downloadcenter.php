<!DOCTYPE html>
<html>
<head>
	<title>Download Center</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />

</head>
	<body>
		<?php
			include "partials/header.php";
			include "partials/downloadCenterContent.php";
			include "partials/footer.php";
		?>


	</body>
</html>
