<!DOCTYPE html>
<html>
<head>
	<title>Contact</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
</head>
	<body>
		<?php
			include "partials/header.php";
			include "partials/content-contact.php";
			include "partials/footer.php";
		?>
	</body>
</html>
