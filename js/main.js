$('.tabs').each(function(){
	$container = $(this);
	setTabHeigt($container);

	$mobileControl = $('<div class="mobile-control"></div>');
	$controlHeader = $('<div class="active-item"></div>');
	$controlHeader.on('click',function(){
		$mobileControl.toggleClass('open');
	});
	$mobileControl.append($controlHeader);
	setControlHeader($container,$controlHeader);
	$controlContent = $('<div class="control-content"></div>');
	$mobileControl.append($controlContent);

	$container.before($mobileControl);

	$('.tab',$container).each(function(){

		$(this).on('click',function(){
			$(this).siblings().removeClass('active');
			$(this).addClass('active');
			setTabHeigt($container);
			setControlHeader($container,$controlHeader);
		});

		$controlElement = $('<a href=""></a>').text($('.tab-header',this).text());

		$controlElement.on('click', (function(event){
			event.preventDefault();
			this.click();
		}).bind(this));

		$controlContent.append($controlElement);
	})
});

function setTabHeigt($tabs) {
	var needed = $tabs.children('.active').children('.tab-header').outerHeight() 
				+ $tabs.children('.active').children('.tab-content').outerHeight();
	$tabs.css('height', needed);
}

function setControlHeader($tabs,$controlHeader) {
	$controlHeader.parent().removeClass('open');
	$controlHeader.text($('.active .tab-header',$tabs).text());
}

//opening selectMenu in product finder
$('.selectMenu').on('click',function(){
	$(this).toggleClass('open');
})
