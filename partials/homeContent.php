<div class="content">
	<?php
		$path = 'partials/content/';
		$contentElements = [
			'flyout.php',
			'stage.php',
      'keyFactsBoxes.php',
			'downloadTeaser.php',
			'text.php',
			'applications.php',
			'productfinder.php',
			'news.php',
			'socialCardsDouble.php',
			'mixed2.php',
			'vcElement.php',
			'contactBox.php',
		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>
