<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'text.php',
			'trainingTeasers.php',
			'seminarFinder.php',
			'resultTable.php',
			'mixed2.php',

		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>