<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'textmediaReverse.php',
			'tabs.php',
			'teasers.php',
			'contactBox.php',
			'mixed3.php'

		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>