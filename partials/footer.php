<footer>
	<nav>
		<ul>
			<li class="socials">
				<div class="socialImages">
					<?php 
						echo '<a href="">' . file_get_contents("img/linkedin.svg") . '</a>';
						echo '<a href="">' . file_get_contents("img/youtube.svg") . '</a>';
						echo '<a href="">' . file_get_contents("img/twitter.svg") . '</a>';
						echo '<a href="">' . file_get_contents("img/facebook.svg") . '</a>';
						echo '<a href="">' . file_get_contents("img/instagram.svg") . '</a>'; 
					?>
				</div>
				<p class="copyright"><span>© </span>2020 Inficon</p>
			</li>
			<li>
				<ul>
					<li><a href="#">Company</a></li>
					<li><a href="#">Market expertise</a></li>
					<li><a href="#">Products</a></li>
					<li><a href="#">Career</a></li>
					<li><a href="#">Services</a></li>
				</ul>
			</li>
			<li>
				<ul>
					<li><a href="#">Investor relations</a></li>
					<li><a href="#">Press</a></li>
				</ul>
			</li>
			<li>
				<ul>
					<li><a href="#">Legal notice</a></li>
					<li><a href="#">Data protection policy</a></li>
					<li><a href="#">Privacy settings</a></li>
					<li><a href="#">Sitemap</a></li>
				</ul>
			</li>
			<li>
				<ul>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Newsletter</a></li>
				</ul>
			</li>
		</ul>
	</nav>

	<script src="js/jquerySlim.min.js"></script> 
	<script src="js/main.js"></script> 
</footer>