<div class="marketTable showBlue">
	<div class="select">
		<select name="" id="">
			<option value="showGreen">green</option>
			<option value="showRed">red</option>
		</select>
	</div>
	<div class="tableHeader">
		<ul>
			<li class="blue">
				<p>Semi & Vacuum Coating</p>
				<div class="icons">
					<a href=""><img title="Display" src="/img/markets/Icon-Markets-Display.svg" alt=""></a>
					<a href=""><img title="Optics" src="/img/markets/Icon-Markets-Optics.svg" alt=""></a>
					<a href=""><img title="Semiconductor" src="/img/markets/Icon-Markets-Semiconductor.svg" alt=""></a>
					<a href=""><img title="Solar" src="/img/markets/Icon-Markets-Solar.svg" alt=""></a>
				</div>
			</li>
			<li class="yellow">
				<p>Security & Energy</p>
				<div class="icons">
					<a href=""><img title="Emergency Response" src="/img/markets/Icon-Markets-Emergency.svg" alt=""></a>
					<a href=""><img title="Environmental Health and Safety" src="/img/markets/Icon-Markets-Environmental-Health.svg" alt=""></a>
					<a href=""><img title="Military" src="/img/markets/Icon-Markets-Military.svg" alt=""></a>
					<a href=""><img title="Alternative Engery and Petrochemical" src="/img/markets/Icon-Markets-Alternative-Energy.svg" alt=""></a>
					<a href=""><img title="Utilities" src="/img/markets/Icon-Markets-Utilities.svg" alt=""></a>
				</div>
			</li>
			<li class="green">
				<p>Refrigeration, Air Conditioning & Automotive</p>
				<div class="icons">
					<a href=""><img title="Refrigeration" src="/img/markets/Icon-Markets-Refrigiration.svg" alt=""></a>
					<a href=""><img title="Air Conditioning" src="/img/markets/Icon-Markets-Air-Conditioning.svg" alt=""></a>
					<a href=""><img title="Automotive" src="/img/markets/Icon-Markets-Automotive.svg" alt=""></a>
					<a href=""><img title="Service Tools" src="/img/markets/Icon-Markets-Service-Tools.svg" alt=""></a>
				</div>
			</li>
			<li class="red">
				<p>General Vacuum</p>
				<div class="icons">
					<a href=""><img title="Research and Development" src="/img/markets/Icon-Markets-Research-Development.svg" alt=""></a>
					<a href=""><img title="Vacuum Furnace and Metallurgy" src="/img/markets/Icon-Markets-Vacuum-Furnace.svg" alt=""></a>
					<a href=""><img title="Industrial Vacuum Coating" src="/img/markets/Icon-Markets-Industrial-Vacuum-Coating.svg" alt=""></a>
					<a href=""><img title="Packaging" src="/img/markets/Icon-Markets-Packaging.svg" alt=""></a>
				</div>
			</li>
		</ul>
	</div>
	<div class="tableData">
		<ul>
			<li>
				<p><a href="">Leak Detectors</a></p>
				<div class="blue">
					<a href="" class="marker"></a>
					<a href="" class="marker"></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
				</div>
				<div class="yellow">
					<a href="" class="marker"></a>
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
					<a href="" class=""></a>
				</div>
				<div class="green">
					<a href="" class=""></a>
					<a href="" class="marker"></a>
					<a href="" class=""></a>
					<a href="" class=""></a>
				</div>
				<div class="red">
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
					<a href="" class="marker"></a>
				</div>
			</li>
			<li>
				<p><a href="">Service Tools for HVAC/R and Automotive</a></p>
				<div class="blue">
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
					<a href="" class="marker"></a>
				</div>
				<div class="yellow">
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
				</div>
				<div class="green">
					<a href="" class="marker"></a>
					<a href="" class="marker"></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
				</div>
				<div class="red">
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
					<a href="" class=""></a>
				</div>
			</li>
			<li>
				<p><a href="">Chemical Detection and Monitoring (GCMS/GC/IR)</a></p>
				<div class="blue">
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class=""></a>
				</div>
				<div class="yellow">
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
					<a href="" class=""></a>
				</div>
				<div class="green">
					<a href="" class="marker"></a>
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class="marker"></a>
				</div>
				<div class="red">
					<a href="" class="marker"></a>
					<a href="" class=""></a>
					<a href="" class=""></a>
					<a href="" class=""></a>
				</div>
			</li>
		</ul>
	</div>
</div>