<form>
	<div class="comboInput">
		<div class="checkbox">
			<input id="uniqueId1" name="uniqueId1" type="checkbox">
			<label for="uniqueId1"></label>
		</div>
		<div class="select">
			<select name="" id="">
				<option value="1">1</option>
				<option value="2">2</option>
			</select>
		</div>
		<div class="info">
			<p><span>Auto Test</span> - Best Practices for Leak Detection in Automotive Applications</p>
		</div>
	</div>
	<div class="comboInput">
		<div class="checkbox">
			<input id="uniqueId2" name="uniqueId2" type="checkbox">
			<label for="uniqueId2"></label>
		</div>
		<div class="select">
			<select name="" id="">
				<option value="1">1</option>
				<option value="2">2</option>
			</select>
		</div>
		<div class="info">
			<p><span>Auto Test</span> - Best Practices for Leak Detection in Automotive Applications</p>
		</div>
	</div>
	<div class="threeColumns">
		<div class="text">
			<input type="text" placeholder="Lastname">
		</div>
		<div class="text">
			<input type="text" placeholder="Country">
		</div>
		<div class="text">
			<input type="text" placeholder="Company">
		</div>
	</div>
	<div class="twoColumns">
		<div class="text">
			<input type="text" placeholder="Phone">
		</div>
		<div class="text">
			<input type="text" placeholder="E-Mail">
		</div>
	</div>

	<div class="checkbox">
		<input id="uniqueId3" name="uniqueId3" type="checkbox">
		<label for="uniqueId3">
			Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
		</label>
	</div>
	<div class="submitWrapper">
		<button class="submitButton button" type="submit">Send</button>
	</div>
</form>