<div class="contentElement space-before-small">
	<div class="contactFinder">
		<h3>Get in Contact with a member of your local spare parts team:</h3>	
		<div class="threeColumns">
			<div class="select">
				<select name="" id="">
					<option value="ger">Germany</option>
				</select>
			</div>
		</div>
		<div class="contactFinderContactBox">
			<div class="contactHeader">
				<p><span>Inficon</span> GmbH</p>
			</div>
			<div class="contactContent">
				<div class="address">
					<p>Bonner Str.498</p>
					<p>50968 Cologne</p>
					<p>Germany</p>
				</div>
				<div class="hotline">
					<p>Service Hotline: <a href="tel:004922156788112">+49 221-56788-112</a></p>
				</div>
				<div class="mail">
					<p>E-Mail: <a href="mailto:service.koeln@inficon.com">service.koeln@inficon.com</a></p>
				</div>
			</div>
		</div>
	</div>
</div>