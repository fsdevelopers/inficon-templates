<div class="contentElement space-after-small">
	<table class="withBorders">
		<tr class="first">
			<th>Date</th>
			<th>Title</th>
			<th>Training type</th>
			<th>Language</th>
			<th>Location</th>
			<th class="buttonRow"></th>
		</tr>
		<tr>
			<td>2020/11/11</td>
			<td>Lorem</td>
			<td>Training type</td>
			<td>German</td>
			<td>Cologne</td>
			<td class="buttonRow"><a href="">Show Details</a></td>
		</tr>
		<tr>
			<td>2020/11/11</td>
			<td>Lorem</td>
			<td>Training type</td>
			<td>German</td>
			<td>Cologne</td>
			<td class="buttonRow"><a href="">Show Details</a></td>
		</tr>
		<tr>
			<td>2020/11/11</td>
			<td>Lorem</td>
			<td>Training type</td>
			<td>German</td>
			<td>Cologne</td>
			<td class="buttonRow"><a href="">Show Details</a></td>
		</tr>
	</table>
</div>