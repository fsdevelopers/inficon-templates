<div class="contentElement space-after-small">
	<div class="twoColumns smallGap">
		<div class="infoBoxes">
			<?php 
				include 'infobox_gray.php';
				include 'infobox.php';
			?>
		</div>
		<div class="eventBox">
			<?php 
				include 'eventBox_arrows.php';
			?>
		</div>
	</div>
</div>