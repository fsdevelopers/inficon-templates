<div class="downloads space-after-small boxGray">
	<ul class="downloadBlocks">
		<li>
			<h4>Operating Manuals</h4>
			<ul class="downloadList">
				<li>
					<p>Leak Testing in the Automotive Industry</p>
					<div class="downloadOptions">
						<select name="" id="">
							<option value="">Language</option>
							<option value="">DE</option>
							<option value="">EN</option>
						</select>
						<a class="button downloadSingle" href=""></a>
					</div>
				</li>
				<li>
					<p>XL3000flex</p>
					<div class="downloadOptions">
						<select name="" id="">
							<option value="">Language</option>
							<option value="">DE</option>
							<option value="">EN</option>
						</select>
						<a class="button downloadSingle" href=""></a>
					</div>
				</li>
				<li>
					<p>Operation Manual XL3000flex</p>
					<div class="downloadOptions">
						<select name="" id="">
							<option value="">Language</option>
							<option value="">DE</option>
							<option value="">EN</option>
						</select>
						<a class="button downloadSingle" href=""></a>
					</div>
				</li>
			</ul>
		</li>
		<li>
			<h4>Broshures</h4>
			<ul class="downloadList">
				<li>
					<p>Declaration of contamination</p>
					<div class="downloadOptions">
						<select name="" id="">
							<option value="">Language</option>
							<option value="">DE</option>
							<option value="">EN</option>
						</select>
						<a class="button downloadSingle" href=""></a>
					</div>
				</li>
				<li>
					<p>USA - Pipeline Survey -<br />All you need to get started</p>
					<div class="downloadOptions">
						<select name="" id="">
							<option value="">Language</option>
							<option value="">DE</option>
							<option value="">EN</option>
						</select>
						<a class="button downloadSingle" href=""></a>
					</div>
				</li>
				<li>
					<p>Leak Testing in the Automotive Industry</p>
					<div class="downloadOptions">
						<select name="" id="">
							<option value="">Language</option>
							<option value="">DE</option>
							<option value="">EN</option>
						</select>
						<a class="button downloadSingle" href=""></a>
					</div>
				</li>
			</ul>
		</li>
	</ul>
	<a class="button buttonBlue" href="">Close</a>
</div>