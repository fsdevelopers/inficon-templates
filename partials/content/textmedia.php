<div class="textMedia space-before-large space-after-small reverse ">
		<!-- add class .reverse for image on the left and text on the right to .textMedia element and .noImage for when there is no image. -->
	<div class="textMediaContent"> 
		<h1>Automotive</h1>
		<p><strong>Lorem ipsum dolor amet bicycle rights green juice tbh, franzen cronut tilde celiac lomo. Waistcoat forage church-key tattooed intelligentsia. Brunch hexagon echo park.</strong></p>
		<p>Lorem ipsum dolor amet bicycle rights green juice tbh, franzen cronut tilde celiac lomo. Waistcoat forage church-key tattooed intelligentsia. Brunch hexagon echo park, bicycle rights street art sustainable stumptown next level biodiesel yuccie. Banjo mumblecore iPhone VHS fanny pack subway tile man braid quinoa taxidermy green juice.</p>
		<ul>
			<li>test</li>
		</ul>
	</div>
	<div class="image">
		<img class="hideMobile" src="img/Image.png" alt="">
		<p>infotext under image</p>
	</div>
</div>