<div class="flyout">
	<div class="flyoutIcon">
		<a href=""><img src="/img/contact-mail.svg" alt=""></a>
	</div>
	<div class="flyoutIcon">
		<a href=""><img src="/img/share.svg" alt=""></a>
		<!-- click here to give #socialOverlay the class .open, then anywhere outside of .socialOverlayContent to remove class .open -->
	</div>
</div>
<div id="socialOverlay" class="">
	<div class="socialOverlayContent">
		<img src="/img/share.svg" alt="" class="shareIcon">
		<h3>Share</h3>
		<p><a href="">www.inficon.com</a></p>
		<ul class="socialList">
			<li>
				<a href="">
					<svg class="linkedIn" xmlns="http://www.w3.org/2000/svg" width="70" height="70.001" viewBox="0 0 70 70.001">
					  <path id="Path_20" data-name="Path 20" d="M152.076,1137H97.648v70h70v-70Zm-13.048,34.933c-3.713,0-4.284,2.9-4.284,5.9V1189.2h-7.136v-22.973h6.848v3.139h.1a7.5,7.5,0,0,1,6.756-3.711c7.23,0,8.564,4.758,8.564,10.944v12.6h-7.133v-11.171C142.741,1175.364,142.691,1171.936,139.028,1171.936Zm-15.9,17.264h-7.139v-22.973h7.139Zm-3.569-26.114a4.139,4.139,0,1,1,4.138-4.141A4.14,4.14,0,0,1,119.562,1163.086Z" transform="translate(-97.648 -1137.003)"/>
					</svg>
					<span>LinkedIn</span>
				</a>
			</li>
			<li>
				<a href="">
					<svg class="youtube" id="Group_7" data-name="Group 7" xmlns="http://www.w3.org/2000/svg" width="70" height="70" viewBox="0 0 70 70">
					  <path id="Path_17" data-name="Path 17" d="M242.492,1186.792l9.924-5.143-9.926-5.179Z" transform="translate(-211.234 -1147.044)" />
					  <path id="Path_18" data-name="Path 18" d="M270.77,1137.2h-70v70h70Zm-15.965,33.549v2.889a57.727,57.727,0,0,1-.38,6.162s-.372,2.623-1.514,3.778a5.441,5.441,0,0,1-3.814,1.613c-5.327.385-13.327.4-13.327.4s-9.9-.09-12.944-.383c-.847-.159-2.749-.11-4.2-1.628-1.141-1.155-1.513-3.778-1.513-3.778a57.506,57.506,0,0,1-.381-6.162v-2.889a57.507,57.507,0,0,1,.381-6.161s.372-2.623,1.513-3.778a5.447,5.447,0,0,1,3.815-1.613c5.327-.385,13.319-.385,13.319-.385h.016s7.992,0,13.319.385a5.446,5.446,0,0,1,3.814,1.613c1.142,1.155,1.514,3.778,1.514,3.778A57.728,57.728,0,0,1,254.805,1170.752Z" transform="translate(-200.77 -1137.203)"/>
					</svg>

					<span>Youtube</span>
				</a>
			</li>
			<li>
				<a href="">
					<svg class="twitter" xmlns="http://www.w3.org/2000/svg" width="70" height="70.001" viewBox="0 0 70 70.001">
					  <path id="Path_19" data-name="Path 19" d="M311.509,1137.2v70h70v-70Zm48.779,28.862c0,9.381-7.15,20.215-20.215,20.215a20.212,20.212,0,0,1-10.877-3.172,13.707,13.707,0,0,0,1.69.1,14.248,14.248,0,0,0,8.826-3.049,7.1,7.1,0,0,1-6.637-4.932,6.994,6.994,0,0,0,1.33.124,6.909,6.909,0,0,0,1.871-.249,7.119,7.119,0,0,1-5.695-6.969v-.1a7.187,7.187,0,0,0,3.214.887,7.134,7.134,0,0,1-2.2-9.491,20.174,20.174,0,0,0,14.645,7.427,7.9,7.9,0,0,1-.179-1.621,7.107,7.107,0,0,1,12.289-4.864,14.138,14.138,0,0,0,4.517-1.718,7.149,7.149,0,0,1-3.132,3.935,14.415,14.415,0,0,0,4.088-1.122,14.472,14.472,0,0,1-3.548,3.686C360.289,1165.456,360.289,1165.761,360.289,1166.065Z" transform="translate(-311.509 -1137.203)"/>
					</svg>

					<span>Twitter</span>
				</a>
			</li>
			<li>
				<a href="">
					<svg class="facebook" xmlns="http://www.w3.org/2000/svg" width="70" height="70.001" viewBox="0 0 70 70.001">
					  <path id="Path_16" data-name="Path 16" d="M489.041,1172.205h0v-35h-70v70h70Zm-26.4-4.549-.778,6.036h-5.2v15.486h-6.232v-15.486h-5.211v-6.036h5.211v-4.45c0-5.165,3.155-7.978,7.762-7.978a42.624,42.624,0,0,1,4.657.238v5.4h-3.2c-2.506,0-2.991,1.191-2.991,2.938v3.853Z" transform="translate(-419.041 -1137.203)"/>
					</svg>

					<span>Facebook</span>
				</a>
			</li>
			<li>
				<a href="">
					<svg class="instagram" id="Group_8" data-name="Group 8" xmlns="http://www.w3.org/2000/svg" width="70" height="70" viewBox="0 0 70 70">
					  <ellipse id="Ellipse_1" data-name="Ellipse 1" cx="5.748" cy="5.748" rx="5.748" ry="5.748" transform="translate(29.252 29.251)" fill="#24517f"/>
					  <path id="Path_21" data-name="Path 21" d="M585.9,1168.495a5.7,5.7,0,0,0-3.272-3.272,9.506,9.506,0,0,0-3.2-.594c-1.819-.083-2.364-.1-6.968-.1s-5.15.017-6.968.1a9.507,9.507,0,0,0-3.2.594,5.7,5.7,0,0,0-3.272,3.272,9.53,9.53,0,0,0-.594,3.2c-.083,1.819-.1,2.364-.1,6.97s.018,5.149.1,6.969a9.529,9.529,0,0,0,.594,3.2,5.707,5.707,0,0,0,3.272,3.273,9.494,9.494,0,0,0,3.2.594c1.818.083,2.363.1,6.968.1s5.15-.017,6.968-.1a9.494,9.494,0,0,0,3.2-.594,5.707,5.707,0,0,0,3.272-3.273,9.525,9.525,0,0,0,.594-3.2c.083-1.819.1-2.365.1-6.969s-.018-5.15-.1-6.97A9.526,9.526,0,0,0,585.9,1168.495Zm-13.443,19.026a8.856,8.856,0,1,1,8.855-8.855A8.855,8.855,0,0,1,572.462,1187.521Zm9.2-15.992a2.069,2.069,0,1,1,2.07-2.069A2.069,2.069,0,0,1,581.667,1171.53Z" transform="translate(-537.462 -1143.667)"/>
					  <path id="Path_22" data-name="Path 22" d="M531,1137.2v70h70v-70Zm52.765,42.369a13.122,13.122,0,0,1-.831,4.338,9.136,9.136,0,0,1-5.227,5.227,13.093,13.093,0,0,1-4.339.831c-1.906.087-2.514.108-7.368.108s-5.462-.021-7.369-.108a13.092,13.092,0,0,1-4.338-.831,9.138,9.138,0,0,1-5.227-5.227,13.119,13.119,0,0,1-.831-4.338c-.086-1.906-.107-2.514-.107-7.369s.021-5.463.107-7.369a13.119,13.119,0,0,1,.831-4.337,9.142,9.142,0,0,1,5.227-5.228,13.113,13.113,0,0,1,4.338-.83c1.907-.087,2.515-.107,7.369-.107s5.462.02,7.368.107a13.114,13.114,0,0,1,4.339.83,9.14,9.14,0,0,1,5.227,5.228,13.122,13.122,0,0,1,.831,4.337c.086,1.906.107,2.515.107,7.369S583.851,1177.666,583.765,1179.573Z" transform="translate(-531 -1137.204)"/>
					</svg>

					<span>Instagram</span>
				</a>
			</li>
			<li>
				<a href="">
					<svg class="whatsapp" id="Group_604" data-name="Group 604" xmlns="http://www.w3.org/2000/svg" width="69.59" height="69.59" viewBox="0 0 69.59 69.59">
					  <path id="Path_189" data-name="Path 189" d="M52.372,47.466a1.914,1.914,0,0,0-3.2.719,1.953,1.953,0,0,1-2.239,1.245A8.856,8.856,0,0,1,41.591,44.1a1.863,1.863,0,0,1,1.231-2.262,1.909,1.909,0,0,0,.72-3.177l-2.875-2.866a2.046,2.046,0,0,0-2.77,0l-1.95,1.95c-1.95,2.029.21,7.454,5.031,12.284s10.26,7.087,12.311,5.036l1.951-1.951a2.047,2.047,0,0,0,0-2.77Z" transform="translate(-10.714 -10.72)" />
					  <path id="Path_190" data-name="Path 190" d="M0,69.59H69.59V0H0Zm50.09-24.7a18.314,18.314,0,0,1-25.371,5.2l-7.046,2.253,2.29-6.8a18.314,18.314,0,1,1,30.128-.652" />
					</svg>

					<span>WhatsApp</span>
				</a>
			</li>
			<li>
				<a href="">
					<svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" viewBox="0 0 70 70">
				  <g id="Group_605" data-name="Group 605" transform="translate(-1245 -561)">
				    <rect id="Rectangle_362" data-name="Rectangle 362" width="70" height="70" transform="translate(1245 561)" />
				    <g id="Icon_feather-copy" data-name="Icon feather-copy" transform="translate(1261 577)">
				      <path id="Path_187" data-name="Path 187" d="M16.743,13.5H31.334a3.243,3.243,0,0,1,3.243,3.243V31.334a3.243,3.243,0,0,1-3.243,3.243H16.743A3.243,3.243,0,0,1,13.5,31.334V16.743A3.243,3.243,0,0,1,16.743,13.5Z" transform="translate(0.849 0.849)" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
				      <path id="Path_188" data-name="Path 188" d="M7.864,24.077H6.243A3.243,3.243,0,0,1,3,20.834V6.243A3.243,3.243,0,0,1,6.243,3H20.834a3.243,3.243,0,0,1,3.243,3.243V7.864" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
				    </g>
				  </g>
				</svg>

					<span>Copy</span>
				</a>
			</li>
			<li>
				<a href="">
					<svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" viewBox="0 0 70 70">
					  <g id="Group_607" data-name="Group 607" transform="translate(-1269 -561)">
					    <g id="Group_606" data-name="Group 606" transform="translate(24)">
					      <rect id="Rectangle_362" data-name="Rectangle 362" width="70" height="70" transform="translate(1245 561)"/>
					    </g>
					    <g id="Icon_feather-mail" data-name="Icon feather-mail" transform="translate(1286 578)">
					      <path id="Path_191" data-name="Path 191" d="M6,6H30a3.009,3.009,0,0,1,3,3V27a3.009,3.009,0,0,1-3,3H6a3.009,3.009,0,0,1-3-3V9A3.009,3.009,0,0,1,6,6Z" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
					      <path id="Path_192" data-name="Path 192" d="M33,9,18,19.5,3,9" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/>
					    </g>
					  </g>
					</svg>

					<span>Mail</span>
				</a>
			</li>
		</ul>
	</div>
</div>