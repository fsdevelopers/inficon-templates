<div class="downloadArea">
	
	<div class="downloadAreaSideBar">
		<div class="selectBox">
			<select name="" id="">
				<option value="">Market</option>
				<option value="">This</option>
				<option value="">That</option>
			</select>
		</div>

		<h4>Product Category</h4>

		<ul class="categories">
			<li class="active"><a href="">Leak Detectors</a></li>  <!-- on click toggle class '.active' -->
			<li><a href="">Service Tools for HVAC/R and Automotive</a></li>  
			<li><a href="">Quartz Crystal</a></li>  
			<li><a href="">RF Sensing Technology</a></li>		  
		</ul>
	</div>
	<div class="downloadAreaContent">
		<div class="downloads space-after-small">
			<ul class="downloadBlocks">
				<li>
					<h4>Operating Manuals</h4>
					<ul class="downloadList">
						<li>
							<p>Leak Testing in the Automotive Industry</p>
							<div class="downloadOptions">
								<select name="" id="">
									<option value="">Language</option>
									<option value="">DE</option>
									<option value="">EN</option>
								</select>
								<a class="button downloadSingle" href=""></a>
							</div>
						</li>
						<li>
							<p>XL3000flex</p>
							<div class="downloadOptions">
								<select name="" id="">
									<option value="">Language</option>
									<option value="">DE</option>
									<option value="">EN</option>
								</select>
								<a class="button downloadSingle" href=""></a>
							</div>
						</li>
						<li>
							<p>Operation Manual XL3000flex</p>
							<div class="downloadOptions">
								<select name="" id="">
									<option value="">Language</option>
									<option value="">DE</option>
									<option value="">EN</option>
								</select>
								<a class="button downloadSingle" href=""></a>
							</div>
						</li>
					</ul>
				</li>
				<li>
					<h4>Broshures</h4>
					<ul class="downloadList">
						<li>
							<p>Declaration of contamination</p>
							<div class="downloadOptions">
								<select name="" id="">
									<option value="">Language</option>
									<option value="">DE</option>
									<option value="">EN</option>
								</select>
								<a class="button downloadSingle" href=""></a>
							</div>
						</li>
						<li>
							<p>USA - Pipeline Survey -<br />All you need to get started</p>
							<div class="downloadOptions">
								<select name="" id="">
									<option value="">Language</option>
									<option value="">DE</option>
									<option value="">EN</option>
								</select>
								<a class="button downloadSingle" href=""></a>
							</div>
						</li>
						<li>
							<p>Leak Testing in the Automotive Industry</p>
							<div class="downloadOptions">
								<select name="" id="">
									<option value="">Language</option>
									<option value="">DE</option>
									<option value="">EN</option>
								</select>
								<a class="button downloadSingle" href=""></a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>