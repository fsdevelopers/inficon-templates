<div class="methodBox">
	<div class="methodBoxHeader">
		<p class="title">Bubble Test</p>
		<a href="">Show details</a> <!-- toggle class .open for .methodBox -->
	</div>

	<div class="textMedia">
		<div class="image">
			<img class="hideMobile" src="img/method.png" alt="">
		</div>
		<div class="textMediaContent"> 
			<p>Lorem ipsum dolor amet bicycle rights green juice tbh, franzen cronut tilde celiac lomo. Waistcoat forage church-key tattooed intelligentsia. Brunch hexagon echo park.</p>
			<p>Lorem ipsum dolor amet bicycle rights green juice tbh, franzen cronut tilde celiac lomo. Waistcoat forage church-key tattooed intelligentsia. Brunch hexagon echo park, bicycle rights street art sustainable stumptown next level biodiesel yuccie. Banjo mumblecore iPhone VHS fanny pack subway tile man braid quinoa taxidermy green juice.</p>
		</div>
	</div>
</div>