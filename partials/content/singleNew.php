<div class="singleNew">
	<div class="date">04/14/2020</div>
	<h4>New Videos: Sniffer Leak Testing and Testing Against Water Ingress (IP67)</h4>
	<p>In our series of animations from INFICON about the different leak detection methods the second video explains Sniffer Leak Testing and how and when it should be used. Also have a look at our new video on <span class="hidden-text">Leak testing against water ingress (IP67).- Conference Call at 09:30 a.m. CEST</span></p>
	<button class="more-button">Read more</button>
</div>