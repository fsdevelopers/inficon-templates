<div class="filter">
	<div class="filterContent">
		<div class="filterHeader">
			<h3>Download finder</h3>
			<div class="results">- 12 Results for <span class="italic">Atomotive</span></div>
			<a href="#" class="reset">Reset filter</a>
		</div>
		<div class="filters">
			<select name="topic" id="automotive">
				<option value="topic">Types of Topic</option>
			</select>
			<select name="type" id="">
				<option value="document">Document Type</option>
				<option value="somethingelse">Something else</option>
			</select>
			<select name="category" id="">
				<option value="product">product</option>
				<option value="mymethod">my method</option>
			</select>
			<a href="" class="button buttonBlue">Show results</a>
		</div>
	</div>
</div>