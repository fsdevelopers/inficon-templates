<div class="filter">
	<div class="filterContent">
		<h3>Product finder <a class="reset" href="">- Reset filter</a></h3>
		<input type="text" placeholder="Search for Product">
		<div class="filters">
			
			<div class="selectMenu"> <!-- on click toggle class 'open' -->
				<span>Market</span>
				<div class="selectMenuContent">
					<ul>
						<li class="active"><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
					</ul>
				</div>
			</div>

			<div class="selectMenu"> <!-- on click toggle class 'open' -->
				<span>Market</span>
				<div class="selectMenuContent">
					<ul>
						<li class="active"><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Air Conditioning</a></li>
					</ul>
				</div>
			</div>
			<select name="method" id="">
				<option value="leaktesting">Leak testing method</option>
				<option value="mymethod">my method</option>
			</select>
			<a href="" class="button buttonBlue">Show results</a>
		</div>
	</div>
</div>
<div class="filterSelection">
	<div class="filterSelectionWrapper">
		<div class="filterSelectionHeader">
			<p><strong>Your Selection</strong></p>
			<p class="results">212 results</p>
		</div>
		<div class="filterSelectionSelection">
			<ul class="categories">
				<li class="category">
					<p>Market</p>
					<ul class="selection">
						<li><a href="">Automotive</a></li>
						<li><a href="">Air Conditioning</a></li>
						<li><a href="">Military</a></li>
					</ul>
				</li>
				<li class="category">
					<p>Application</p>
					<ul class="selection">
						<li><a href="">Vacuum Controllers</a></li>
					</ul>
				</li>
				<li class="category">
					<p>Product</p>
					<ul class="selection">
						<li><a href="">VDG500</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>