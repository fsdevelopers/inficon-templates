<div class="stage">
  <div class="slider_track">
    <a class="slider">
      <div class="slide">
        <img src="img/stage.png" alt="">
        <div class="stageOverlay">
          <div class="stageOverlayText">
            <p>“The automobile market is developing faster than ever. That’s why it’s particularly <b>important</b> to listen closely and to immediately respond to new customers’ needs.”</p>
          </div>
        </div>
      </div>
    </a>
  </div>

	<div class="stageProgressBars">
		<div class="stageProgressBar active">
			<p class="stageProgressBarText">Service</p>
			<div class="stageProgressBarBar"><span data-progress="10" class="stageProgressBarFiller"></span></div>
		</div>
		<div class="stageProgressBar">
			<p class="stageProgressBarText">Automotive</p>
			<div class="stageProgressBarBar"><span data-progress="0" class="stageProgressBarFiller"></span></div>
		</div>
		<div class="stageProgressBar">
			<p class="stageProgressBarText">Career</p>
			<div class="stageProgressBarBar"><span data-progress="0" class="stageProgressBarFiller"></span></div>
		</div>
	</div>
</div>
