<div class="contentElement space-before-large">
	<div class="background">
		<div class="greyboxContent">
			<div class="icon">
				<img src="/img/greyIcon.png" alt="">
			</div>
			<div class="text">
				<h4>Maintenance</h4>
				<p><strong>Keep your device running with a planned interruption only!</strong></p>
				<p>Reduce the risk of unexpected device failure by having your devices proactively checked!</p>
			</div>
		</div>
	</div>
</div>	