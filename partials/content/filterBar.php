<div class="filterBar">
	<div class="filterBarWrapper">
		<ul class="filterBarFilters">
			<li class="filterBarFilter active">
				<a href="">Operating Manuals</a>
			</li>
			<li class="filterBarFilter">
				<a href="">Brochures</a>
			</li>
			<li class="filterBarFilter">
				<a href="">Catalogs</a>
			</li>
			<li class="filterBarFilter">
				<a href="">Application Notes</a>
			</li>
		</ul>
	</div>
</div>