<div class="contentElement teaserDates open">
	<a href="" class="showMore">Show details</a>  <!-- toggles class '.open' to div.teaserDates-->
	<div class="teaserDateHead">	
		<img src="/img/exhibition.png" alt="" class="teaserDateIcon">
	
		<div class="teaserDateHeader">
			<h4 class="name">Japan Vacuum</h4>
			<p class="location">Okyo big sight, Japan</p>
		</div>
		<div class="teaserDateDates">
			<p class="date dateFrom">October 14, 2020</p>
			<p class="date dateTo">October 16, 2020</p>
		</div>

		<ul class="tags">
			<li>Webinar</li>
			<li>English</li>
			<li class="share"></li>
		</ul>
	</div>
	<div class="teaserDateContent">
		<h4>Fast results</h4>
		<p>
			The improved vacuum system with low maintenance vacuum pumps allows the detection of leaks starting from atmospheric pressure. The rugged mass spectrometer system ensures long running time and low maintenance costs.
		</p>
		<img class="teaserDateImage" src="/img/teaserDateImage.png" alt="">
		<h4>Rotatable Display and remote controllable user interface</h4>
		<p>
			The improved vacuum system with low maintenance vacuum pumps allows the detection of leaks starting from atmospheric pressure. The rugged mass spectrometer system ensures long running time and low maintenance costs.
		</p>
		<div class="buttons">
			<a href="" class="button">Ticket Order</a>
			<a href="" class="button">Arrange Appointment</a>
		</div>
	</div>
</div>