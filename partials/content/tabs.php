<div class="tabs-container space-before-small space-after-large">
	<div class="tabs">
		<div class="tab active">
			<div class="tab-header">
				Specifications
			</div>
			<div class="tab-content">
				<table>
					<tr>
						<th>Type</th>
						<td>ELT3000</td>
					</tr>
					<tr>
						<th>Smallest detectable leak rate</th>
						<td>1x10-60mbar l/s (Helium equivalent leak rate)</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="tab">
			<div class="tab-header">
				Part Number
			</div>
			<div class="tab-content">
				<table>
					<tr>
						<th>2</th>
						<td>ELT3000</td>
					</tr>
					<tr>
						<th>Smallest detectable leak rate</th>
						<td>1x10-60mbar l/s (Helium equivalent leak rate)</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="tab">
			<div class="tab-header">
				Accessories
			</div>
			<div class="tab-content">
				<table>
					<tr>
						<th>3</th>
						<td>ELT3000</td>
					</tr>
					<tr>
						<th>Smallest detectable leak rate</th>
						<td>1x10-60mbar l/s (Helium equivalent leak rate)</td>
					</tr>
					<tr>
						<th>Smallest detectable leak rate</th>
						<td>1x10-60mbar l/s (Helium equivalent leak rate)</td>
					</tr>
					<tr>
						<th>Smallest detectable leak rate</th>
						<td>1x10-60mbar l/s (Helium equivalent leak rate)</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>