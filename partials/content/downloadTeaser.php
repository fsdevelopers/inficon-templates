<div class="downloadTeaser space-after-large">
	<div class="downloadTeaserWrapper">
		<img src="img/leak_testing.png" alt="">
		<div class="downloadTeaserContent">
			<h4>Leak Testing of Electric Drive Motors</h4>
			<p>The market for electric vehicles is growing rapidly. Electric drive motors are typically exposed to water either from the environment or during high pressure water cleaning in cat wash stations. More and more</p>
			<div class="downloadTeaserContentButtons">
				<a href="" class="button">Download</a>
				<a href="" class="button">Got to Info Center</a>
			</div>
		</div>
	</div>
</div>