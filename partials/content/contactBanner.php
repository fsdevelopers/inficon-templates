<div class="contentElement">
	<div class="contactBanner">
		<div class="contactBannerImage">
			<img src="img/germany.png" alt="">
		</div>
		<div class="contactBannerContent">
			<div class="contentHeader">
				<p><span>Inficon</span> GmbH</p>
			</div>
			<div class="twoColumns">
				<div class="address">
					<p>Bonner Straße 498</p>
					<p>D-50968 Köln</p>
					<p>Deutschland</p>
				</div>
				<div class="contact">
					<p>Tel: <a href="tel:0049221567880">+49.221.56788-0</a></p>
					<p>Fax: <a href="fax:0049221567880">+49.221.56788-90</a></p>
					<p>E-Mail: <a href="mailto:reach.germany@inficon.com">reach.germany@inficon.com</a></p>
				</div>
			</div>
			<a href="" class="button buttonBlue">Show in GoogleMaps</a>
		</div>
	</div>
</div>