<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'map.php',
			'scrollButton.php',
			'contactBanner.php',
			'backgroundTeaser.php',
			'sparePartFinder.php',
			'contactFinder.php',
			'contactForm.php',
		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>