<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'text.php',
			'video.php',
			'productfinder.php',
			'teasers.php',
			'greyBackground.php',
			'teasers.php',
			'teasersWithPagination.php',
			'contactBox.php',

		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>