<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'text.php',
			'productfinder.php',
			'productTeasers.php',
			'contactBox.php',

		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>