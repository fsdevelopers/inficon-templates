<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'marketTable.php'
		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>