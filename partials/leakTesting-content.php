<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'stage.php',
			'methodBoxes.php',
			'contactBox.php',
		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>