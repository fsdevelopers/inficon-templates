<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'stage.php',
			'filterBar.php',
			'downloadArea.php',

		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>