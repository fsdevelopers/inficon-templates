<div class="submenuFooter">
	<div class="socials">
		<?php 
			echo '<a href="">' . file_get_contents("img/linkedin.svg") . '</a>';
			echo '<a href="">' . file_get_contents("img/youtube.svg") . '</a>';
			echo '<a href="">' . file_get_contents("img/twitter.svg") . '</a>';
			echo '<a href="">' . file_get_contents("img/facebook.svg") . '</a>';
			echo '<a href="">' . file_get_contents("img/instagram.svg") . '</a>'; 
		?>
	</div>
	<ul>
		<li><a href="">Legal notice</a></li>
		<li><a href="">Data protection policy</a></li>
		<li><a href="">Privacy Settings</a></li>
		<li><a href="">Sitemap</a></li>
	</ul>
	<p><sup>©</sup>2020 Inficon</p>
</div>