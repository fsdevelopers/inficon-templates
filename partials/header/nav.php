<div id="mainNav">
	<div class="logo">
		<a href="/index.php"><img src="img/Logo.svg" alt=""></a>
	</div>
	<nav>
		<ul>
			<li><a href="">Applications</a></li>
			<li><a href="/product.php">Products</a>
				<?php include "partials/header/submenu.php"?>
			</li>
			<li><a href="">Leak testing method</a>
				<?php include "partials/header/submenu.php"?>
			</li>
			<li><a href="">Infocenter Automotive</a></li>
			<li><a href="">Case Studies</a></li>
			<li id="search">
				<img src="img/Search.svg" alt="">
				<input type="search">
			</li>
		</ul>
	</nav>
	<div class="mobileNavToggle">
		<div></div>
		<div></div>
		<div></div>
	</div>
	<!-- add class .open to #mobileNav-->
</div>

<!-- initial state 
	after click on a link direct to that page. on click on a a.next add class open to li.submenu and the ul inside of it. Also add class hidden to all other li.submenu on that level.

-->
<div id="mobileNav" class="">
	<nav>
		<div id="searchMobile">
			<img src="img/Search.svg" alt="">
			<input type="search">
		</div>
		<ul class="">
			<li class="submenu ">
				<a href="">Company 1</a><a href="" class="next"></a>
				<ul class="">
					<li class="previous"><a href="">Markets</a></li>
					<li><a href="">Company</a></li>
					<li><a href="">Company</a></li>
					<li><a href="">Company</a></li>
				</ul>
			</li>
			<li class="submenu ">
				<a href="">Company 2</a><a href="" class="next"></a>
				<ul></ul>
			</li>
			<li class="submenu ">
				<a href="">Company 3</a><a href="" class="next"></a>
				<ul></ul>
			</li>
			<li class="submenu ">
				<a href="">Company 4</a><a href="" class="next"></a>
				<ul></ul>
			</li>
		</ul>
	</nav>
	<ul class="info">
		<li><a href="">Contact</a></li>
		<li><a href="">Info Center</a></li>
		<li><a href="">Product Finder</a></li>
		<li><a href="">Login</a></li>
	</ul>
	<div class="showMore">
		<a class="showMoreButton" href="#">Show More</a>
		<ul>
			<li><a href="">Legal Notice</a></li>
			<li><a href="">Data protection policy</a></li>
			<li><a href="">Privacy settings</a></li>
			<li><a href="">Sitemap</a></li>
		</ul>
		<div class="socials">
			<?php 
				echo '<a href="">' . file_get_contents("img/linkedin.svg") . '</a>';
				echo '<a href="">' . file_get_contents("img/youtube.svg") . '</a>';
				echo '<a href="">' . file_get_contents("img/twitter.svg") . '</a>';
				echo '<a href="">' . file_get_contents("img/facebook.svg") . '</a>';
				echo '<a href="">' . file_get_contents("img/instagram.svg") . '</a>'; 
			?>
		</div>
		<p>©2020 Inficon</p>
	</div>
</div>