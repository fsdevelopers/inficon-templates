<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'stage.php',
			'newsletterForm.php',
		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>