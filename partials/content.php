<div class="content">
	<?php 
		$path = 'partials/content/';
		$contentElements = [
			'stage.php',
			'textmedia.php',
			'keyFactsBoxes.php',
			'backgroundTeaser.php',
			'greybox.php',
			'applications.php',
			'productfinder.php',
			'greyList.php',
			'news.php',
			'socialCards.php',
			'downloadfinder.php',
			'downloads.php',
			'downloadTeaser.php',
			'mixed.php',
			'contactBox.php',

		];

		foreach ($contentElements as $element) {
			include $path.$element;
		}
	?>
</div>